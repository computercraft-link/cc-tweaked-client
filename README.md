# ComputerCraft Link Client for CC:Tweaked

This program for a [CC:Tweaked](https://tweaked.cc) computer connects to the websocket server hosted by [the companion vscode extension](https://gitlab.com/computercraft-link/vscode-extension) and links the two. It is recommended to run this on advanced computers only as it can be run constantly in a background tab without blocking your whole computer.

[[_TOC_]]

## Install
1. Install the [GitLab Cloner](https://gitlab.com/computercraft-link/gitlab-cloner#install)
2. run `> gitlab-cloner --id 37441643`

## Requirements
**This will not work** unless you have [the companion vscode extension](https://gitlab.com/computercraft-link/vscode-extension)

## Usage
`ccLink.lua` takes UNIX-style arguments. If you are just looking to get up and runnning, this is the fastest way:

`> ccLink.lua --url 127.0.0.1:56560 --uuid 1234-5678-9876`

However, this would be annoying to enter often, so luckily there are [settings](#settings) you can configure too

## Arguments

### `--uuid`
**Type: string**

This is the uuid provided by the VS Code extension when a new computer connection is created.

### `--url`
**Type: string**

The URL/IP that the VS Code extension can be found at. If giving an IP, this should always end in `:56560`, this is the port that the extension listens to. In a singleplayer world, this would be `127.0.0.1:56560` but on a dedicated server not hosted on your computer, this will have to be either your [local](https://lifehacker.com/how-to-find-your-local-and-external-ip-address-5833108#h732931 "How do I find my local IP?") or [public IP](https://www.google.com/search?q=my+ip "See yours on Google").

## Settings

### `ccLink.uuid`
**Type: string**

Giving this setting a value will allow you to omit the [`--uuid`](#uuid) argument when calling `ccLink.lua`.

**Example:**

`> set ccLink.uuid 1234-5678-9876`

### `ccLink.url`
**Type: string**

Giving this setting a value will allow you to omit the [`--url`](#url) argument when calling `ccLink.lua`.

The URL/IP that the VS Code extension can be found at. If giving an IP, this should always end in `:56560`, this is the port that the extension listens to. In a singleplayer world, this would be `127.0.0.1:56560` but on a dedicated server not hosted on your computer, this will have to be either your [local](https://lifehacker.com/how-to-find-your-local-and-external-ip-address-5833108#h732931 "How do I find my local IP?") or [public IP](https://www.google.com/search?q=my+ip "See yours on Google").

**Example:**

`> set ccLink.url 127.0.0.1:56560`

### `ccLink.reconnectInterval`
**Type: number**

**Default: 5**

The number of seconds between reconnection attempts should the websocket suddenly lose connection. If the websocket is closed manually by the extension, the program is exited and there will be no reconnection attempts.
