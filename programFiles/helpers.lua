local pretty = require("cc.pretty")

local helpers = {}

---Execute a certain case based on the value given
---@param value string|integer The value to switch between cases with
---@param cases table<string|integer, fun(...)> A table of cases to execute when `value` is equal to the corresponding key
---@param ... any Additional arguments to pass through to the case function
---@return any ... Any returns from the case function
function helpers.switch(value, cases, ...)
	for k, v in pairs(cases) do
		if tostring(k) == tostring(value) then
			return v(...)
		end
	end
end

---Checks if a table includes a value
---@param tbl table The table to check
---@param value any The value to check for in the table
---@return boolean isIncluded If the value is in the table
function helpers.includes(tbl, value)
	for k, v in pairs(tbl) do
		if v == value then
			return true
		end
	end
	return false
end

---@class flag
---@field type "boolean"|"string"|"number"
---@field default boolean|string|number
---@field pattern? "string"

---Parses argument flags
---@param flags table<string, flag> A table of recognized flags
---@param args table The provided args
---@return table<string, string|boolean|number>|nil value The flag values or `nil` if there was an error
---@return string? error The reason why the flags couldn't be parsed
function helpers.parseFlags(flags, args)
	local results = {}

	for i = 1, #args do
		local name = string.match(args[i], "^%-%-(%w[%w+%-]*)")

		if name then
			if not flags[name] then
				pretty.print(
                    pretty.group(
                        pretty.concat(
                            pretty.text("Unknown flag:", colors.orange),
                            pretty.space_line,
                            pretty.text(name, colors.magenta),
                            pretty.text(" is not a known flag", colors.white)
                        )
                    ),
                    1
                )
            else
                local requestedType = flags[name].type

                if requestedType == "boolean" then
                    results[name] = true
                elseif requestedType == "string" then
                    local nextArg = args[i + 1]
                    local isFlag = (string.find(nextArg or "", "^%-%-")) ~= nil

                    if not nextArg or isFlag then
                        local err = pretty.concat(
                            pretty.text("Invalid flag:", colors.red),
                            pretty.space_line,
                            pretty.text("--" .. name, colors.magenta),
                            pretty.text(" is not a boolean flag and expects to be followed by a string", colors.white)
                        )
                        return nil, err
                    end

                    -- string pattern validation
                    if flags[name].pattern then
                        if string.find(nextArg, flags[name].pattern) == nil then
                            local err = pretty.concat(
                                pretty.text("Invalid flag:", colors.red),
                                pretty.space_line,
                                pretty.text("--" .. name, colors.magenta),
                                pretty.text(" does not match the expected pattern", colors.white)
                            )
                            return nil, err
                        end
                    end

                    results[name] = nextArg
                    i = i + 1
                elseif requestedType == "number" then
                    local nextArg = args[i + 1]
                    local isFlag = (string.find(nextArg or "", "^%-%-")) ~= nil

                    if not nextArg or isFlag then
                        local err = pretty.concat(
                            pretty.text("Invalid flag:", colors.red),
                            pretty.space_line,
                            pretty.text("--" .. name, colors.magenta),
                            pretty.text(" is not a boolean flag and expects to be followed by a number", colors.white)
                        )
                        return nil, err
                    end

                    local asNum = tonumber(nextArg)
                    if not asNum then
                        local err = pretty.concat(
                            pretty.text("Invalid flag:", colors.red),
                            pretty.space_line,
                            pretty.text("--" .. name, colors.magenta),
                            pretty.text(" expects to be followed by a number", colors.white)
                        )
                        return nil, err
                    end

                    results[name] = asNum
                    i = i + 1
                end
			end

		end
	end

	return results
end

return helpers
