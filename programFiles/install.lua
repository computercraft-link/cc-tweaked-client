local ccLinkSettings = require("settings")
local pathToHere = shell.getRunningProgram()

ccLinkSettings.defineSettings()

-- Help file setup
local path = help.path()
local pathToHelp = fs.getDir(pathToHere) .. "/help/"

if not string.find(path, pathToHelp) then
	help.setPath(help.path() .. ":" .. pathToHelp)
end
